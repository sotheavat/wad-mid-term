let root = document.getElementById("root");

window.onload = () => {
  if (!localStorage.getItem("data")) {
    localStorage.setItem("data", JSON.stringify(credential));
  }
  if (!localStorage.getItem("table-data")) {
    localStorage.setItem("table-data", JSON.stringify(ticketListing));
  }
};

let onSummit = (isSignUp) => {
  let err = document.getElementById("error-msg");
  let usr = document.getElementById("usr");
  let pwd = document.getElementById("password");

  let isFound = false;
  JSON.parse(localStorage.getItem("data")).map((val) => {
    if (val.username == usr.value.trim() && pwd.value.trim() == val.password) {
      isFound = true;
      if (isSignUp) {
        err.style.display = "block";
      } else {
        localStorage.setItem("user", JSON.stringify(val));
      }
    }
  });
  if (!isFound && isSignUp) {
    if (usr.value.trim() == "" || pwd.value.trim() == "") {
      err.style.display = "block";
      console.log("no found and signup");
    } else {
      let dataN = JSON.stringify([
        ...Array.from(JSON.parse(localStorage.getItem("data"))),
        {
          username: usr.value.trim(),
          password: pwd.value.trim(),
          role: "user",
        },
      ]);

      localStorage.setItem("data", dataN);
      localStorage.setItem(
        "user",
        JSON.stringify({
          username: usr.value.trim(),
          password: pwd.value.trim(),
          role: "user",
        })
      );
      setTimeout(function () {
        window.location.reload();
      });
    }
  }
  if (!isFound && !isSignUp) {
    console.log("not found and not signup");
    err.style.display = "block";
  } else if (isFound && !isSignUp) {
    console.log("not found and signup end");
    setTimeout(function () {
      window.location.reload();
    });
  }
};

let signUp = () => {
  document.getElementById("l-root").innerHTML = ` <h1>Sign up</h1>
        <input type="text" placeholder="Username" id="usr"/>
        <input type="password" placeholder="Password" id="password"/>
        <small
        id="error-msg"
        class="form-text text-muted text-danger"
        style="display: none"
      >
        Invalid Cridential
      </small>
        <button class="btn-light-blue btn-40" onclick="onSummit(true)">Sign up</button>`;
  document.getElementById("o-root").innerHTML = ` <div
          class="
            overlay-panel overlay-left
            d-flex
            flex-column
            justify-content-center
            align-items-center
            h-100
          "
        >
          <h1 class="my-3">Hello, User!</h1>
          <p class="my-1">
            Enter Your Cridential Details and start using our service Or
          </p>
          <button class="btn-light-blue btn-40 my-4" onclick="signIn()">
            Sign In
          </button>
        </div>`;
};

let signIn = () => {
  document.getElementById("l-root").innerHTML = ` <h1>Sign In</h1>
  <input type="text" placeholder="Username" id="usr"/>
        <input type="password" placeholder="Password" id="password"/>
        <small
        id="error-msg"
        class="form-text text-muted text-danger"
        style="display: none"
      >
        Invalid Cridential
      </small>
        <a href="#">Forgot your password?</a>
        <button class="btn-light-blue btn-40" onclick="onSummit(false)">Sign In</button>`;
  document.getElementById("o-root").innerHTML = `            <div
          class="
          overlay-panel overlay-left
            d-flex
            flex-column
            justify-content-center
            align-items-center
            h-100
          "
        >
          <h1 class="my-3">Welcome Back!</h1>
          <p class="my-1">To Keep Using our service please login Or</p>
          <button class="btn-light-blue my-4 btn-40" onclick="signUp()">Sign Up</button>
        </div>`;
};

let signInPage = () => {
  $("#table_id_wrapper").hide();
  $("#root").css("height", "100vh");
  root.innerHTML = `<div class="w-50 h-100 d-flex align-items-center m-auto" id="s-id">
  <div
    class="w-100 d-flex justify-content-center h-50 m-auto border-style"
    id="l-container"
  >
    <div class="form-container sign-container">
      <form
        action="#"
        class="
          d-flex
          flex-column
          justify-content-evenly
          align-items-center
          w-100
          h-100
        "
        id="l-root"
      >
      <h1>Sign In</h1>
      <input type="text" placeholder="Username" id="usr"/>
            <input type="password" placeholder="Password" id="password"/>
            <small
            id="error-msg"
            class="form-text text-muted text-danger"
            style="display: none"
          >
            Invalid Cridential
          </small>
            <a href="#">Forgot your password?</a>
            <button class="btn-light-blue btn-40" onclick="onSummit(false)">Sign In</button>
      </form>
    </div>
    <div
      class="
        overlay-container
        h-100
        d-flex
        align-items-center
        justify-content-center
      "
    >
      <div class="overlay" id="o-root">
      <div
      class="
      overlay-panel overlay-left
        d-flex
        flex-column
        justify-content-center
        align-items-center
        h-100
      "
    >
      <h1 class="my-3">Welcome Back!</h1>
      <p class="my-1">To Keep Using our service please login Or</p>
      <button class="btn-light-blue my-4 btn-40" onclick="signUp()">Sign Up</button>
    </div>
      </div>
    </div>
  </div>
</div>`;
};
