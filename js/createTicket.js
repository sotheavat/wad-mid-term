function insertTicket() {
  let title = document.getElementById("inputTitle").value;
  let Rtype = document.getElementById("Rtype").value;
  let des = document.getElementById("Textarea").value;
  let temptable = JSON.parse(localStorage.getItem("table-data"));
  localStorage.setItem(
    "table-data",
    JSON.stringify([
      ...temptable,
      {
        ID: (Math.random() + 1).toString(36).substring(7),
        Title: title,
        Description: des,
        RequestID: "1",
        Status: "New",
        Type: Rtype,
        RequestedDate: new Date().toISOString().substring(0, 10),
        CompletedDate: "",
        Action: "Button",
      },
    ])
  );
  var table = $("#table_id").DataTable();
  table.destroy();
  Table(JSON.parse(localStorage.getItem("table-data")));
  $("#table_id_wrapper").show();
  document.getElementById("inputTitle").value = "";
  document.getElementById("Rtype").value = "";
  document.getElementById("Textarea").value = "";
  $.notify("Ticket Create Successfully!", "success");
}
let create = document.getElementById("main");
let createTicket = () => {
  document
    .querySelector("#navbarNav > ul > li:nth-child(1) > div")
    .classList.remove("active");
  document
    .querySelector("#navbarNav > ul > li:nth-child(2) > div")
    .classList.add("active");
  document
    .querySelector("#navbarNav > ul > li:nth-child(3) > div")
    .classList.remove("active");
  $("#table_id_wrapper").hide();
  $("#root").css("height", "100vh");
  create.innerHTML = `
  <div class="w-100 h-100 d-flex justify-content-center align-items-center">
  <div class="inbox-content w-form">
  <form id="myForm">
  <div class="mb-3">
    <label for="inputTitle" class="form-label">Title</label>
    <label style="color:#dc3545;">*</label>
    <input type="text" class="form-control inputWidth" id="inputTitle" autocomplete="off">
  </div>
  <div class="mb-3">
  <label for="requsttype" class="form-label">Requst type</label>
  <select class="form-select inputWidth" id="Rtype" aria-label="Default select example">
  <option hidden disabled selected value>Choose one...</option>
  <option value="General">General question</option>
  <option value="Feature">Feature request</option>
  <option value="Bugs">Bugs</option>
  <option value="My account">My account</option>
  <option value="Other">Other</option>
  </select>
  </div>
  <div class="mb-3">
  <label for="Textarea" class="form-label">Description</label>
  <textarea class="form-control " id="Textarea" rows="5"></textarea>
  </div>
  <div class="btn btn-light-blue"  onclick="insertTicket()">Submit</div>
  </form>
  </div>
  </div>`;
};
