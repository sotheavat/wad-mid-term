let profile = document.getElementById("pf");
let managebutton = document.getElementById("managebutton");
let listUser = JSON.parse(localStorage.getItem("user"));

let errorAdminFeature = () => {
  $.notify("This is for admin feature only", "error");
};

if (localStorage.getItem("user")) {
  profile.innerHTML = `
  <div class="btn user pf border-sd-2" id="pf-content">
  <div class="d-none cus-dropdown h-15 d-flex flex-column justify-content-evenly align-items-center" id="cus-dropdown">
    <div class="btn-light-blue btn-40 h-100 w-75 d-flex justify-content-center align-items-center bg-logout"><h5 onclick="clearData()">Log Out</h5></div>
  </div>
</div>`;

  // check if admin or not (if admin manage tickets can access)
  if (listUser.role == "admin") {
    managebutton.innerHTML = `
        <div class="btn h-50 btn-create d-flex justify-content-center align-items-center " onclick="createTicket()">
          <div class="btn">
            <h3>Create Ticket</h3>
          </div>
        </div>
        <div class="btn h-50 btn-manage d-flex justify-content-center align-items-center " onclick="manageTicket()">
          <div class="btn" >
            <h3>Manage Ticket</h3>
          </div>
        </div>
      `;
    document.getElementById("navbarNav").innerHTML = `
      <ul class="navbar-nav nav-l">
      <li class="nav-item">
        <div class="nav-link active" aria-current="page" onclick="refresh()">Home</div>
      </li>
      <li class="nav-item">
        <div class="nav-link" onclick="createTicket()">Ticket</div>
      </li>
      <li class="nav-item">
        <div class="nav-link" onclick="manageTicket()">Manage</div>
      </li>
      <li class="nav-item" id="pf">
      <div class="btn user pf border-sd-2" id="pf-content">
      <div class="d-none cus-dropdown h-15 d-flex flex-column justify-content-evenly align-items-center" id="cus-dropdown">
        <div class="btn-light-blue btn-40 h-100 w-75 d-flex justify-content-center align-items-center bg-logout"><h5 onclick="clearData()">Log Out</h5></div>
      </div>
    </div>
      </li>
    </ul>
      `;
    profile.innerHTML = `
      <div class="btn user pf border-sd-2" id="pf-content">
      <div class="d-none cus-dropdown h-15 d-flex flex-column justify-content-evenly align-items-center" id="cus-dropdown">
        <div class="btn-light-blue btn-40 h-100 w-75 d-flex justify-content-center align-items-center bg-logout"><h5 onclick="clearData()">Log Out</h5></div>
      </div>
    </div>`;
  } else {
    managebutton.innerHTML = `
        <div class="btn h-50 btn-create d-flex justify-content-center align-items-center " onclick="createTicket()">
          <div class="btn" >
            <h3>Create Ticket</h3>
          </div>
        </div>
        <div class="btn h-50 btn-manage d-flex justify-content-center align-items-center " onclick="errorAdminFeature()">
          <div class="btn" >
            <h3>Manage Ticket</h3>
          </div>
        </div>
  
      `;
    document.getElementById("navbarNav").innerHTML = `
      <ul class="navbar-nav nav-l">
      <li class="nav-item">
        <div class="nav-link active" aria-current="page" onclick="refresh()">Home</div>
      </li>
      <li class="nav-item">
        <div class="nav-link" onclick="createTicket()">Ticket</div>
      </li>
      <li class="nav-item">
        <div class="nav-link" onclick="errorAdminFeature()">Manage</div>
      </li>
      <li class="nav-item" id="pf">
      <div class="btn user pf border-sd-2" id="pf-content">
      <div class="d-none cus-dropdown h-15 d-flex flex-column justify-content-evenly align-items-center" id="cus-dropdown">
        <div class="btn-light-blue btn-40 h-100 w-75 d-flex justify-content-center align-items-center bg-logout"><h5 onclick="clearData()">Log Out</h5></div>
      </div>
    </div>
      </li>
    </ul>
      `;
  }
} else {
  document.getElementById("navbarNav").innerHTML = `
  <ul class="navbar-nav nav-l">
  <li class="nav-item">
    <div class="nav-link active" aria-current="page" onclick="refresh()">Home</div>
  </li>
  <li class="nav-item">
    <div class="nav-link" onclick="signInPage()">Ticket</div>
  </li>
  <li class="nav-item">
    <div class="nav-link" onclick="signInPage()">Manage</div>
  </li>
  <li class="nav-item" id="pf">
    <div class="nav-link btn btn-light-blue" onclick="signInPage()">
      Sign In
    </div>
  </li>
</ul>
  `;
  profile.innerHTML = `                  <div
    class="nav-link btn btn-light-blue"
    onclick="signInPage()"
  >
    Sign In
  </div>`;
  managebutton.innerHTML = `
    <div class="btn h-50 btn-create d-flex justify-content-center align-items-center " onclick="signInPage()">
      <div class="btn">
        <h3>Create Ticket</h3>
      </div>
    </div>
    <div class="btn h-50 btn-manage d-flex justify-content-center align-items-center " onclick="signInPage()">
      <div class="btn">
        <h3>Manage Ticket</h3>
      </div>
    </div>
  `;
}

let refresh = () => {
  setTimeout(function () {
    window.location.reload();
  });
};

let pf = document.getElementById("pf-content");
let e = document.getElementById("cus-dropdown");
console.log(pf, e);
let handlePfClick = () => {
  if (e.classList.contains("d-none")) {
    e.classList.remove("d-none");
    e.classList.add("d-show");
  } else {
    e.classList.remove("d-show");
    e.classList.add("d-none");
  }
};
if (pf) {
  pf.addEventListener("click", () => handlePfClick());
}
let clearData = () => {
  localStorage.removeItem("user");
  location.reload();
};
