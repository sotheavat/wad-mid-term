let credential = [
  {
    username: "user",
    password: "user",
    role: "user",
  },
  {
    username: "admin",
    password: "admin",
    role: "admin",
  },
];

let ticketListing = [
  {
    ID: "asd12",
    Title: "some bad thing happen here",
    Description: "In report invoice date is wrong",
    RequestID: "1",
    Status: "Open",
    Type: "Bugs",
    RequestedDate: "2021-01-01",
    CompletedDate: "",
    Action: "Button",
  },
  {
    ID: "dse12",
    Title: "wrong date",
    Description: "In report invoice date is wrong",
    RequestID: "1",
    Status: "New",
    Type: "Bugs",
    RequestedDate: "2021-01-01",
    CompletedDate: "",
    Action: "Button",
  },
  {
    ID: "dsje1",
    Title: "wrong date",
    Description: "In report invoice date is wrong",
    RequestID: "1",
    Status: "Close",
    Type: "Bugs",
    RequestedDate: "2021-01-01",
    CompletedDate: "2021-11-12",
    Action: "Button",
  },
];
