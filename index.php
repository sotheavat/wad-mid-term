<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.css"
    integrity="sha512-CJ6VRGlIRSV07FmulP+EcCkzFxoJKQuECGbXNjMMkqu7v3QYj37Cklva0Q0D/23zGwjdvoM4Oy+fIIKhcQPZ9Q=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
  <link rel="stylesheet" href="css/home.css" />
  <link rel="stylesheet" href="css/signIn.css" />
  <link rel="stylesheet" href="css/createTicket.css">
  <script src="js/state.js"></script>
  <title>Complain Management System</title>
  <style>
    #table_id_wrapper {
      display: none;
    }
  </style>
</head>

<body>
  <div id="root">
    <div id="nav" class="container-fluid h-100">
      <nav class=" navbar navbar-expand-lg navbar-light bg-transparent position-absolute px-lg-5 w-100">
        <div class="container-fluid">
          <div class="navbar-brand" id="lg-text" onclick="refresh()">Complain Management System</div>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav nav-l">
              <li class="nav-item">
                <div class="nav-link active" aria-current="page" onclick="refresh()">Home</div>
              </li>
              <li class="nav-item">
                <div class="nav-link" onclick="createTicket()">Ticket</div>
              </li>
              <li class="nav-item">
                <div class="nav-link" onclick="manageTicket()">Manage</div>
              </li>
              <li class="nav-item" id="pf">
                <div class="nav-link btn btn-light-blue" onclick="signInPage()">
                  Sign In
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="h-100 bg-light d-flex justify-content-center align-items-center ">
        <div id="main" class="content-container d-flex w-85 h-75 flex-column flex-xxl-row align-items-center ">
          <div class=" content mx-3 bg-light h-100 w-half d-flex justify-content-center align-items-center flex-column">
            <h1 class="font-bolder my-4 font-extra-huge">
              A
              <span class="text-light-green">Customer Service</span>
              Tool
            </h1>
            <h2 class="font-bolder text-wrap">
              that helps companies manage their
            </h2>
            <h2 class="font-bolder">
              <span class="text-light-green">Service</span> and
              <span class="text-light-green">Support Cases</span>
            </h2>
          </div>
          <div id="managebutton" class="intro mx-3 bg-white h-100 w-half border-sd-1 text-white d-flex flex-column ">
            <div class="btn h-50 btn-create d-flex justify-content-center align-items-center " onclick="createTicket()">
              <div class="btn">
                <h3>Create Ticket</h3>
              </div>
            </div>
            <div class="btn h-50 btn-manage d-flex justify-content-center align-items-center " onclick="manageTicket()">
              <div class="btn">
                <h3>Manage Ticket</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="container bg-light">
    <div class="mt-5">
      <table id="table_id" class="row-border" style="width:100%">
      </table>
    </div>
  </div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
  integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
  integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"
  integrity="sha512-efUTj3HdSPwWJ9gjfGR71X9cvsrthIA78/Fvd/IN+fttQVy7XWkOAXb295j8B3cmm/kFKVxjiNYzKw9IQJHIuQ=="
  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script src="js/home.js"></script>
<script src="js/signIn.js"></script>
<script src="js/manage.js"></script>
<script src="js/createTicket.js"></script>

<script>
  $(document).ready( function () {
    Table(<?php
$servername = "localhost:3306";
$username = "cms-admin";
$password = "supersecure";
$dbName = "cms-db";
$kak = array();
// Create connection
$conn = new mysqli($servername, $username, $password,$dbName);
$sql = "SELECT * FROM ticket";
$result = $conn->query($sql);
$n =0;
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $list[$n] = array(
     "ID" => $row["id"],
     "Title" => $row["title"],
     "Description" => $row["description"],
     "RequestID" => $row["requestId"],
     "Status" => $row["status"],
     "Type" => $row["type"],
     "RequestedDate" => $row["requestDate"],
     "CompletedDate" => $row["completedDate"],
     "Action" => "Button"
    );
    $n++;
}
}
$conn->close();
echo json_encode($list);
?>)
  });
  function Table(data){
    console.log(data)
    $('#table_id').DataTable({
      responsive: true,
      "data": data,
      'columns': [
          {
              "title": "ID",
              "data": "ID"
          },
          {
              "title": "Title",
              "data": "Title"
          },
          {
              "title": "Description",
              "data": "Description"
          },
          {
              "title": "RequestID",
              "data": "RequestID"
          },
          {
              "title": "Status",
              "data": "Status"
          },
          {
            "title": "Type",
            "data": "Type"
          },
          {
              "title": "RequestedDate",
              "data": "RequestedDate"
          },{
              "title": "CompletedDate",
              "data": "CompletedDate"
          },{
            "title": "Action",
            "data": "Action"
        }
      ],
      "rowCallback": function( row, data, index ) {
        console.log("row =",row,"data =",data,"index =", index);
        $('td:eq(8)', row).html( `<div class="btn-group">
          <button type="button" class="btn btn-info dropdown-toggle text-white" data-bs-toggle="dropdown" aria-expanded="false">
            Change Status
          </button>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" onclick="updateStatus('${data.ID}','Open')" href="#">Open</a></li>
            <li><a class="dropdown-item" onclick="updateStatus('${data.ID}','Pending')" href="#">Pending</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" onclick="updateStatus('${data.ID}','Close')" href="#">Close</a></li>
          </ul>
        </div>
        ` );              
          if ( data.Status === "Open" ) {
              $('td:eq(4)', row).html( '<i class="badge bg-light-success text-success" style="background-color: #e8fdeb">Open</i>' );
          }
          else if  (data.Status === "New" ) {
              $('td:eq(4)', row).html( '<i class="badge bg-light-info text-info" style="background-color: #cfecfe">New</i>' );
          }
          else if  (data.Status === "Pending" ) {
            $('td:eq(4)', row).html( '<i class="badge bg-light-warning text-warning" style="background-color: #fff8ec">Pending</i>' );
          }
          else if  (data.Status === "Close" ) {
              $('td:eq(4)', row).html( '<i class="badge bg-light-danger text-danger" style="background-color: #f9e7eb">Close</i>' );
          }
      },
  });

  }
  
</script>

</html>
